FROM python:slim
COPY . .
RUN pip install -r requirements.txt
CMD gunicorn "server:create_app()"