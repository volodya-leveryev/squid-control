from http import HTTPStatus

import pytest

from server import create_app


@pytest.fixture
def client():
    app = create_app()
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client


def test_index_page(client):
    res = client.get('/')
    assert HTTPStatus.OK == res.status_code
    assert b'Hello' in res.data
