from flask import Flask, render_template


def index_page():
    return render_template('index.html')


def create_app():
    app = Flask(__name__)
    app.add_url_rule('/', 'index', index_page)
    return app
