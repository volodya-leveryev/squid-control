#!/bin/bash

DOCKER=docker

if [ -x "$(command -v podman)" ]; then
    DOCKER=podman
fi

$DOCKER build -t squid .
